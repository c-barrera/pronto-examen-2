package com.cisbarey.fs2.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.cisbarey.fs2.model.Insurance;
import com.cisbarey.fs2.model.repository.InsuranceCustomRepository;
import com.cisbarey.fs2.model.repository.InsuranceRepository;
import com.cisbarey.fs2.util.CsvUtils;

@RestController
@RequestMapping("/insurance")
public class InsuranceController {

	@Autowired
	private InsuranceRepository repository;
	@Autowired
	private InsuranceCustomRepository customRepository;

	@CrossOrigin
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public List<Insurance> getAllInsurance() {
		return this.repository.findAll();
	}

	@CrossOrigin
	@RequestMapping(value = "/pages/{pageSize}", method = RequestMethod.GET)
	public Integer getAllPages(@PathVariable("pageSize") int pageSize) {
		int total = Long.valueOf(this.repository.count()).intValue() / pageSize;
		return total + 1;
	}

	@CrossOrigin
	@RequestMapping(value = "/pagination/{pageNumber}/{pageSize}", method = RequestMethod.GET)
	public List<Insurance> getAllInsurancePage(@PathVariable("pageNumber") int pageNumber,
			@PathVariable("pageSize") int pageSize) {
		return this.customRepository.getAllPagination(pageNumber, pageSize);
	}

	@CrossOrigin
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Insurance getInsuranceByPage(@PathVariable("id") ObjectId id) {
		return this.repository.findBy_id(id);
	}

	@CrossOrigin
	@PostMapping(value = "/upload", consumes = "multipart/form-data")
	public void uploadMultipart(@RequestParam("file") MultipartFile file) throws IOException {
		List<Insurance> insurances = CsvUtils.read(Insurance.class, file.getInputStream());
		this.repository.saveAll(insurances);
	}

	@CrossOrigin
	@PostMapping(value = "/upload", consumes = "text/csv")
	public void uploadSimple(@RequestBody InputStream body) throws IOException {
		List<Insurance> insurances = CsvUtils.read(Insurance.class, body);
		this.repository.saveAll(insurances);
	}

}
