package com.cisbarey.fs2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Fs2ApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(Fs2ApiApplication.class, args);
	}

}
