package com.cisbarey.fs2.model.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.cisbarey.fs2.model.Insurance;

@Repository
public class InsuranceCustomRepositoryImpl implements InsuranceCustomRepository {

	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public List<Insurance> getAllPagination(int pageNumber, int pageSize) {
		Query query = new Query();
		query.skip(pageNumber * pageSize);
		query.limit(pageSize);
		return mongoTemplate.find(query, Insurance.class);
	}

}
