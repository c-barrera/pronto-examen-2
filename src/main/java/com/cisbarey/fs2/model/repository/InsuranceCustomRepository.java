package com.cisbarey.fs2.model.repository;

import java.util.List;

import com.cisbarey.fs2.model.Insurance;

public interface InsuranceCustomRepository {

	List<Insurance> getAllPagination(int pageNumber, int pageSize);
}
