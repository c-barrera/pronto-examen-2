package com.cisbarey.fs2.model.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.cisbarey.fs2.model.Insurance;

public interface InsuranceRepository extends MongoRepository<Insurance, String> {

	Insurance findBy_id(ObjectId _id);

}
