package com.cisbarey.fs2.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

public class Insurance {

	@Id
	public ObjectId _id;

	public int policyID;
	public String statecode;
	public String county;
	public double eq_site_limit;
	public double hu_site_limit;
	public double fl_site_limit;
	public double fr_site_limit;
	public double tiv_2011;
	public double tiv_2012;
	public double eq_site_deductible;
	public double hu_site_deductible;
	public double fl_site_deductible;
	public double fr_site_deductible;
	public double point_latitude;
	public double point_longitude;
	public String line;
	public String construction;
	public int point_granularity;

	public Insurance() {
		super();
	}

	public Insurance(ObjectId _id, int policyID, String statecode, String county, double eq_site_limit,
			double hu_site_limit, double fl_site_limit, double fr_site_limit, double tiv_2011, double tiv_2012,
			double eq_site_deductible, double hu_site_deductible, double fl_site_deductible, double fr_site_deductible,
			double point_latitude, double point_longitude, String line, String construction, int point_granularity) {
		super();
		this._id = _id;
		this.policyID = policyID;
		this.statecode = statecode;
		this.county = county;
		this.eq_site_limit = eq_site_limit;
		this.hu_site_limit = hu_site_limit;
		this.fl_site_limit = fl_site_limit;
		this.fr_site_limit = fr_site_limit;
		this.tiv_2011 = tiv_2011;
		this.tiv_2012 = tiv_2012;
		this.eq_site_deductible = eq_site_deductible;
		this.hu_site_deductible = hu_site_deductible;
		this.fl_site_deductible = fl_site_deductible;
		this.fr_site_deductible = fr_site_deductible;
		this.point_latitude = point_latitude;
		this.point_longitude = point_longitude;
		this.line = line;
		this.construction = construction;
		this.point_granularity = point_granularity;
	}

	public String get_id() {
		return _id.toHexString();
	}

	public void set_id(ObjectId _id) {
		this._id = _id;
	}

	public int getPolicyID() {
		return policyID;
	}

	public void setPolicyID(int policyID) {
		this.policyID = policyID;
	}

	public String getStatecode() {
		return statecode;
	}

	public void setStatecode(String statecode) {
		this.statecode = statecode;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public double getEq_site_limit() {
		return eq_site_limit;
	}

	public void setEq_site_limit(double eq_site_limit) {
		this.eq_site_limit = eq_site_limit;
	}

	public double getHu_site_limit() {
		return hu_site_limit;
	}

	public void setHu_site_limit(double hu_site_limit) {
		this.hu_site_limit = hu_site_limit;
	}

	public double getFl_site_limit() {
		return fl_site_limit;
	}

	public void setFl_site_limit(double fl_site_limit) {
		this.fl_site_limit = fl_site_limit;
	}

	public double getFr_site_limit() {
		return fr_site_limit;
	}

	public void setFr_site_limit(double fr_site_limit) {
		this.fr_site_limit = fr_site_limit;
	}

	public double getTiv_2011() {
		return tiv_2011;
	}

	public void setTiv_2011(double tiv_2011) {
		this.tiv_2011 = tiv_2011;
	}

	public double getTiv_2012() {
		return tiv_2012;
	}

	public void setTiv_2012(double tiv_2012) {
		this.tiv_2012 = tiv_2012;
	}

	public double getEq_site_deductible() {
		return eq_site_deductible;
	}

	public void setEq_site_deductible(double eq_site_deductible) {
		this.eq_site_deductible = eq_site_deductible;
	}

	public double getHu_site_deductible() {
		return hu_site_deductible;
	}

	public void setHu_site_deductible(double hu_site_deductible) {
		this.hu_site_deductible = hu_site_deductible;
	}

	public double getFl_site_deductible() {
		return fl_site_deductible;
	}

	public void setFl_site_deductible(double fl_site_deductible) {
		this.fl_site_deductible = fl_site_deductible;
	}

	public double getFr_site_deductible() {
		return fr_site_deductible;
	}

	public void setFr_site_deductible(double fr_site_deductible) {
		this.fr_site_deductible = fr_site_deductible;
	}

	public double getPoint_latitude() {
		return point_latitude;
	}

	public void setPoint_latitude(double point_latitude) {
		this.point_latitude = point_latitude;
	}

	public double getPoint_longitude() {
		return point_longitude;
	}

	public void setPoint_longitude(double point_longitude) {
		this.point_longitude = point_longitude;
	}

	public String getLine() {
		return line;
	}

	public void setLine(String line) {
		this.line = line;
	}

	public String getConstruction() {
		return construction;
	}

	public void setConstruction(String construction) {
		this.construction = construction;
	}

	public int getPoint_granularity() {
		return point_granularity;
	}

	public void setPoint_granularity(int point_granularity) {
		this.point_granularity = point_granularity;
	}

}
